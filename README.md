This repository contains code and data for the project "Die Kanones der Aufklärung: multidisziplinär, multilingual, digital" ('The Canons of Englightenment: multidisciplinary, multilingual, digital') sponsored by the Trier Center of Digital Humanities in the context of the project "MimoText". It contains the following directories:

* [`get metadata`](get_metadata/). This directory  contains the following notebooks: 
    * [`get_metadata_ref.ipynb`](get_metadata/get_metadata_ref.ipynb), to obtain metadata for the reference corpus,
    * [`remove_unwanted.ipynb`](get_metadata/remove_unwanted.ipynb), to clean metadata (remove unwanted keywords and duplicate volumes).
    * [`analyse_ref_corpus.ipynb`](get_metadata/analyse_ref_corpus.ipynb), to examine the temporal distribution of the reference corpus.

* [`analysis ref corpus`](analysis_ref_corpus). This directory contains the following notebooks:
    * [`parsing_NER.ipynb`](analysis_ref_corpus/parsing_NER.ipynb), to create lists of the most frequently mentioned authors in the reference corpus, using output of a Named Entity Algorithm provided by Hathitrust.
    * [`reconc_eng.ipynb`](analysis_ref_corpus/reconc_eng.ipynb),     	 [`reconc_fren.ipynb`](analysis_ref_corpus/reconc_fren.ipynb), and [`reconc_ger.ipynb`](analysis_ref_corpus/reconc_ger.ipynb), notebooks that effect the actual 'reconciliation', i. e. the matching between names we have found and historical actors. For this we use Wikidata.
    * [`analyse_canons.ipynb`](analysis_ref_corpus/analyse_canons.ipynb) which contains some additional data preparation and an analysis of the overlaps between canons in our three target languages and disciplines,
    * [`canon_timelines.ipynb`](analysis_ref_corpus/canon_timelines.ipynb), a notebook containing a visualisation of the lifespans of persons mentioned in the various canons as timelines and a cohort analysis of their composition (dividing persons in the canons into quintiles according to their date of birth).

The creation and analysis of the search corpus is more complex. To manage this complexity, we separate code for each language. Structurally there are many similarities, but we want each notebook to run independently from others to facilitate reproduction of a certain subcorpus. It should also be mentioned that only a small set of actual files from Hathitrust are contained in this repository. Our local repository with all files currently contains approx. 19 GB of data. 

We now describe the overall structure of the language-specific directories and use the French search corpus, [`search_corpus_french`](search_corpus_french/) as our example. The logically first notebook, [`keywords_search_corpus.ipynb`](search_corpus_french/keywords_search_corpus.ipynb), applies to all three search corpora, French, German, and English: it uses the keywords applied to books in the reference corpus to create lists that can be used for the constitution of each search corpus. These lists are used to download bibliographic records for these keywords, separately for literature, philosophy, and other disciplines.

The lists of bibliographic records is then used to identify and deduplicate the individual volumes belonging to these records (e. g. in [`search_corp_fren_lit`](search_corpus_french/search_corp_fren_lit)). The volumes are downloaded (see for an example [`search_fren_lit.ipynb`](search_corpus_french/search_fren_lit)). Each subcorpus is first analysed separately (see e. g. [`analysis_fren_lit.ipynb`](search_corpus_french/analysis_fren_lit.ipynb) and then, with a longer list of canonical authors, in its totality ([`analysis_fren_all`](search_corpus_french/analysis_fren_all)).


